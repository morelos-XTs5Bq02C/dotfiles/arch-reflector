Custom files for Archlinux Reflector

These files are modified to taste.
Read first before deploying!

To deploy, open a terminal in the same directory as this file and then:

~~~shell
sudo ./deploy.sh
~~~

This command requires a sudoer as it copies files to a directory inside
/etc/.

You can check what the script does by opening it in a text editor.

Files will be sent to their respective destinations as specified 
by the Arch Wiki.

If you need them elsewhere, modify deploy.sh to your needs.
